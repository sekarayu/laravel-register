<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register Form</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style>
        body {
            padding-top: 100px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                @if (session('status'))
                    <div id="alert_msg" class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form action="{{ url('register') }}" method="POST">

                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Name</label>
                        <input id="user_name" type="text" name="name" class="form-control" value="{{ old('name')}}">

                        <small id="name_msg" class="text-danger">{{ $errors->first('name') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email Address</label>
                        <input id="user_email" type="email" name="email" class="form-control" value="{{ old('email')}}">

                        <small id="email_msg" class="text-danger">{{ $errors->first('email') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>
                        <input id="user_password" type="password" name="password" class="form-control" value="{{ old('password')}}">

                        <small id="pass_msg" class="text-danger">{{ $errors->first('password') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                        <label for="password-confirm">Confirmation Password</label>
                        <input id="user_pass_confirm" type="password" name="password-confirm" class="form-control" value="{{ old('password-confirm')}}">

                        <small id="conf_pass_msg" class="text-danger">{{ $errors->first('password-confirm') }}</small>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" id="submitbtn">Register</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</body>
</html>