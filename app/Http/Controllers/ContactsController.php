<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactsController extends Controller
{
    //
    public function showContactForm()
	{
    	return view('register');
	}

	public function register(Request $request)
	{
        $rules = [
            'name' => 'required|min:5|max:30|alpha',
            'email' => 'required|email',
            'password' => 'required',
            'password-confirm' => 'required|same:password',
        ];

        $this->validate($request, $rules);

        $custid = rand(1,30);

        // register

        return back()->with("status", "Your account has been created successfully! Account ID : ".$custid);
	}
}
